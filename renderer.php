<?php

defined('MOODLE_INTERNAL') || die();

class block_cegep_renderer extends \plugin_renderer_base {

    /**
     * Enrol a coursegroup into a Moodle course (admin function)
     */
    public function page_enrol_admin() {
        global $CFG, $DB, $USER, $enroldb, $sisdb;
        require($CFG->dirroot . '/blocks/cegep/block_cegep_enrol_admin_form.php');

        $course = $this->page->course;
        $context = $this->page->context;
        $returnurl = $this->page->url;

        // Set up enrolment form
        $enrolform = new cegep_enrol_admin_form(new moodle_url($CFG->wwwroot . '/blocks/cegep/block_cegep_enrolment.php', array( 'a' => 'enrol', 'id' => $course->id)), array('course_idnumber' => $course->idnumber, 'course_visible' => $course->visible, 'user_idnumber' => $USER->idnumber));

        $currenttab = 'enrol';
        $out = '';

        // Go back to course page if cancelled
        if ($enrolform->is_cancelled()) {
            redirect(new moodle_url($CFG->wwwroot . '/course/view.php', array('id' => $course->id)));
        }
        // Process validated data
        elseif ($data = $enrolform->get_data(false)) {

            // Extract course code info
            if (empty($data->coursecode)) {
                $coursecode = substr($course->idnumber, 0, strripos($course->idnumber, '_'));
            } else {
                $coursecode = $data->coursecode;
            }

            // Extract term info
            $term = $data->year . $data->semester;

            if (strstr($data->coursegroup, ',')) {
                $coursegroups = explode(',', $data->coursegroup);
            } else {
                $coursegroups = array($data->coursegroup);
            }

            $students_enrolled = array();
            foreach ($coursegroups as $coursegroup) {
                // Enrol coursegroup
                if (!$s = cegep_local_enrol_coursegroup($coursecode, $coursegroup, $term)) {
                    print_error('errorimportingstudentlist','block_cegep', $returnurl);
                } else {
                    $students_enrolled = array_merge($students_enrolled, $s);
                }

                // Create Moodle group
                if (isset($data->creategroups) && $data->creategroups) {
                    $groupname = sprintf('%s gr.%d %s', $coursecode, $coursegroup, cegep_local_term_to_string($term));
                    $group = $DB->get_record('groups', array('name' => $groupname), '*');
                    if (is_object($group) && isset($group->id)) {
                        $groupid = $group->id;
                    }
                    else {
                        $group = new stdClass();
                        $group->courseid = $course->id;
                        $group->name = $groupname;
                        $groupid = groups_create_group($group);
                    }
                    foreach ($s as $username) {
                        $u = $DB->get_record('user', array('username' => $username), 'id', MUST_EXIST);
                        groups_add_member($groupid, $u->id);
                    }
                }
            }

            if (!$course->visible && isset($data->makevisible) && $data->makevisible) {
                $course->visible = 1;
                $DB->update_record('course', $course);
            }

            // Display nice confirmation with student list and buttons
            $out .= $this->header();
            $out .= $this->get_tabs($context, $currenttab, array('id' => $course->id));
            $out .= $this->heading(get_string('enrol','block_cegep'), 3);
            $out .= $this->container(get_string('coursegroupenrolled','block_cegep',implode($students_enrolled,'<br />')), 'center');
            $linkyes = new moodle_url($CFG->wwwroot.'/blocks/cegep/block_cegep_enrolment.php', array('a' => 'enrol', 'id' => $course->id));
            $linkno = new moodle_url($CFG->wwwroot.'/course/view.php', array('id' => $course->id));
            $out .= $this->confirm(get_string('enrolanother','block_cegep'), $linkyes, $linkno);
        }
        // Display the enrolment form
        else {
            $out .= $this->header();
            $out .= $this->get_tabs($context, $currenttab, array('id' => $course->id));
            $out .= $this->heading(get_string('enrol','block_cegep'), 3);
            $out .= $enrolform->render();
        }

        $out .= $this->footer($course);
        return $out;
    }

     /*
     * This function is aimed at teachers enrolling a section into
     * a course. It restricts them to enrolling sections
     * that the DB says they are teaching.
     *
     */
    public function page_enrol() {
        global $CFG, $DB, $USER, $enroldb, $sisdb;
        require($CFG->dirroot . '/blocks/cegep/block_cegep_enrol_form.php');

        $course = $this->page->course;
        $context = $this->page->context;
        $returnurl = $this->page->url;

        // Set up enrolment form
        $enrolform = new cegep_enrol_form(new moodle_url($CFG->wwwroot . '/blocks/cegep/block_cegep_enrolment.php', array( 'a' => 'enrol', 'id' => $course->id)), array('course_idnumber' => $course->idnumber, 'user_idnumber' => $USER->idnumber));

        $currenttab = 'enrol';
        $body = '';
        $out = '';

        // Go back to course page if cancelled
        if ($enrolform->is_cancelled()) {
            redirect(new moodle_url($CFG->wwwroot . '/course/view.php', array('id' => $course->id)));
        }
        // Process validated data
        elseif ($data = $enrolform->get_data(false)) {
            $students_enrolled = array();

            foreach ($data->coursegroup as $coursegroup_id) {
                // Enrol selected coursegroup(s)
                if (!$se = cegep_local_enrol_coursegroup($coursegroup_id)) {
                    print_error('errorimportingstudentlist','block_cegep');
                }

                // Create Moodle group
                if (isset($data->creategroups) && $data->creategroups) {
                    $coursegroup = cegep_local_get_coursegroup($coursegroup_id);
                    $groupname = sprintf('%s gr.%d %s', $coursegroup['coursecode'], $coursegroup['group'], cegep_local_term_to_string($coursegroup['term']));
                    $group = $DB->get_record('groups', array('courseid' => $course->id, 'name' => $groupname), '*');
                    if (is_object($group) && isset($group->id)) {
                        $groupid = $group->id;
                    }
                    else {
                        $group = new stdClass();
                        $group->courseid = $course->id;
                        $group->name = $groupname;
                        $groupid = groups_create_group($group);
                    }
                    foreach ($se as $username) {
                        $u = $DB->get_record('user', array('username' => $username), 'id', MUST_EXIST);
                        groups_add_member($groupid, $u->id);
                    }
                }

                $students_enrolled += $se;
            }

            // Display nice confirmation with student list and buttons
            $body .= $this->container(get_string('coursegroupenrolled','block_cegep',implode($students_enrolled,'<br />')), 'center');
            $linkyes = new moodle_url($CFG->wwwroot.'/blocks/cegep/block_cegep_enrolment.php', array('a' => 'enrol', 'id' => $course->id));
            $linkno = new moodle_url($CFG->wwwroot.'/course/view.php', array('id' => $course->id));
            $body .= $this->confirm(get_string('enrolanother','block_cegep'), $linkyes, $linkno);
        }
        // Display the enrolment form
        else {
            $body .= $enrolform->render();
        }

        $out .= $this->header();
        $out .= $this->get_tabs($context, $currenttab, array('id' => $course->id));
        $out .= $this->heading(get_string('enrol','block_cegep'), 3);
        $out .= $body;
        $out .= $this->footer();

        return $out;
    }

    /**
     * Unenrol a coursegroup from a Moodle course
     */
    public function page_unenrol() {
        global $CFG, $DB, $USER, $enroldb, $sisdb;
        require($CFG->dirroot . '/blocks/cegep/block_cegep_unenrol_form.php');

        $course = $this->page->course;
        $context = $this->page->context;
        $returnurl = $this->page->url;

        // Set up enrolment form
        $unenrolform = new cegep_unenrol_form(new moodle_url($CFG->wwwroot . '/blocks/cegep/block_cegep_enrolment.php', array( 'a' => 'unenrol', 'id' => $course->id)), array('course_idnumber' => $course->idnumber, 'user_idnumber' => $USER->idnumber));

        $currenttab = 'unenrol';
        $body = '';
        $out = '';

        // Go back to course page if cancelled
        if ($unenrolform->is_cancelled()) {
            redirect(new moodle_url($CFG->wwwroot . '/course/view.php', array('id' => $course->id)));
        }
        // Process validated data
        elseif ($data = $unenrolform->get_data()) {

            $coursegroup_list = implode(', ', $data->coursegroup);

            // Get usernames before removing
            $select = "SELECT `$CFG->enrol_remoteuserfield` FROM `$CFG->enrol_dbname`.`$CFG->enrol_remoteenroltable` WHERE `$CFG->enrol_remotecoursefield` = '$course->idnumber' AND `$CFG->enrol_remoterolefield` = '$CFG->block_cegep_studentrole' AND `coursegroup_id` IN ($coursegroup_list);";

            $usernames = $enroldb->Execute($select)->getRows();

            // If user exists, unassign role right away
            if ($usernames) {
                $student_role = $DB->get_record('role', array($CFG->enrol_localrolefield => $CFG->block_cegep_studentrole));
                $enrol = enrol_get_plugin('database');
                if ($instance = $DB->get_record('enrol', array('courseid' => $course->id, 'enrol' => 'database'), '*', IGNORE_MULTIPLE)) {
                    foreach ($usernames as $username) {
                        if ($student_user = $DB->get_record('user', array($CFG->enrol_localuserfield => $username[$CFG->enrol_remoteuserfield]), 'id')) {
                            $enrol->unenrol_user($instance, $student_user->id);
                        }
                    }
                }
            }

            // Go through each coursegroup and remove Moodle external enrolment database record
            $delete = "DELETE FROM `$CFG->enrol_remoteenroltable` WHERE `$CFG->enrol_remotecoursefield` = '$course->idnumber' AND `$CFG->enrol_remoterolefield` = '$CFG->block_cegep_studentrole' AND `coursegroup_id` IN ($coursegroup_list);";

            $result = $enroldb->Execute($delete);

            if (!$result) {
                trigger_error($enroldb->ErrorMsg() .' STATEMENT: '. $delete);
                print_error('errordeletingenrolment','block_cegep');
            } else {
                $message = get_string('coursegroupunenrolled','block_cegep',$enroldb->Affected_Rows());
                $n = new \core\output\notification($message, \core\output\notification::NOTIFY_SUCCESS, false);
                $n->set_show_closebutton();
                $body .= $this->render($n);
                $body .= $this->continue_button($returnurl);
            }
        }
        // Display the unenrolment form
        else {
            $body .= $unenrolform->render();
        }

        $out .= $this->header();
        $out .= $this->get_tabs($context, $currenttab, array('id' => $course->id));
        $out .= $this->heading(get_string('unenrol','block_cegep'), 3);
        $out .= $body;
        $out .= $this->footer();

        return $out;
    }

    /**
     * Course self-creation form for teachers
     */
    public function page_createcourse($coursecode) {
        global $CFG, $USER;
        require($CFG->dirroot . '/blocks/cegep/block_cegep_createcourse_form.php');

        $coursetitle = cegep_local_get_course_title($coursecode);

        $body = '';
        $out = '';

        if ($coursetitle === false) {
            $message = get_string('coursecodeinvalid', 'block_cegep');
            $n = new \core\output\notification($message, \core\output\notification::NOTIFY_ERROR, false);
            $n->set_show_closebutton();
            $body .= $this->render($n);
            $body .= $this->continue_button($CFG->wwwroot);
        }
        else {
            // Set up form
            $createcourse_form = new cegep_createcourse_form(null, array('coursecode' => $coursecode, 'coursetitle' => $coursetitle));

            // Go back to dashboard if cancelled
            if ($createcourse_form->is_cancelled()){
                redirect($CFG->wwwroot);
            }
            // Process validated data
            elseif ($data = $createcourse_form->get_data()) {
                require_once('../../course/lib.php');

                $course = cegep_local_new_course_template($data->coursecode);
                $course->shortname = $course->idnumber;
                $course->fullname = $data->fullname;
                $course->visible = $data->visible;

                $course = create_course($course);

                if (is_object($course) && $course->id > 0) {
                    // Enrol current user (teacher) into the new course (except if admin)
                    if (!is_siteadmin($USER)) {
                        cegep_local_enrol_user($course->idnumber, $USER->username, 'editingteacher');
                    }
                    // Redirect to the enrolment form
                    redirect($CFG->wwwroot.'/blocks/cegep/block_cegep_enrolment.php?a=enrol&id=' . $course->id, get_string('coursecreatesuccess','block_cegep'), null, \core\output\notification::NOTIFY_SUCCESS);
                } else {
                    print_error('errorcreatingcourse','block_cegep');
                }
            }
            // Display the course creation form
            else {
                $body .= $createcourse_form->render();
            }
        }

        $out .= $this->header();
        $out .= $this->heading(get_string('coursecreate','block_cegep'), 3);
        $out .= $body;
        $out .= $this->footer();

        return $out;
    }


    /**
     * List all coursegroups and students enrolled in this Moodle course
     */
    public function page_studentlist() {
        global $CFG, $DB, $enroldb, $sisdb;

        $course = $this->page->course;
        $context = $this->page->context;

        $currenttab = 'studentlist';
        $body = '';
        $out = '';

        $courses = array($course);

        if ($courses) {

            foreach ($courses as $c) {

                if (empty($c->idnumber)) {
                    $c = $DB->get_record('course', array('id'=> $c->id));
                }

                $select = "SELECT DISTINCT `program_idyear` FROM `$CFG->enrol_remoteenroltable` WHERE `$CFG->enrol_remotecoursefield` = '$c->idnumber' AND `program_idyear` IS NOT NULL";

                $program_idyears = $enroldb->Execute($select);

                $select = "SELECT DISTINCT `coursegroup_id` FROM `$CFG->enrol_remoteenroltable` WHERE `$CFG->enrol_remotecoursefield` = '$c->idnumber' AND `coursegroup_id` IS NOT NULL";

                $coursegroups = $enroldb->Execute($select);

                if (!$coursegroups && !$program_idyears) {
                    trigger_error($enroldb->ErrorMsg() .' STATEMENT: '. $select);
                }
                else {

                    $program_idyear = '';
                    $coursegroup_id = '';

                    while (!$program_idyears->EOF) {

                        $program_idyear = $program_idyears->fields['program_idyear'];
                        $program_idyear = explode('_', $program_idyear);

                        $program_id = $program_idyear[0];
                        $program_year = $program_idyear[1];

                        // Get title
                        $select = "SELECT `title` FROM `$CFG->sisdb_name`.`program` WHERE `id` = '$program_id'";
                        $program = $sisdb->Execute($select)->fields;

                        $notice = '';

                        $notice .= '<strong>'.get_string('program','block_cegep').'</strong> : ' . $program_id . ' - ' . $program['title'] . '<br />';
                        $notice .= '<strong>'.get_string('programyear','block_cegep').'</strong> : ' . get_string('programyear'.$program_year,'block_cegep');

                        $select = "SELECT * FROM `$CFG->enrol_remoteenroltable` WHERE `$CFG->enrol_remotecoursefield` = '$c->idnumber' AND `program_idyear` = '".$program_id."_".$program_year."' AND `$CFG->enrol_remoterolefield` = '$CFG->block_cegep_studentrole' ORDER BY `$CFG->enrol_remoteuserfield` ASC";

                        $table = $this->get_studentlist_enrolmenttable($select);

                        if (count($table->data) > 0) {
                            $notice .= html_writer::table($table);
                            $notice .= '<br /><strong>'.get_string('total').'</strong> : ' . count($table->data);
                        } else { ($notice .= get_string('nostudentsenrolled', 'block_cegep') . "<br /><br />"); }

                        $out .= $this->box_start();
                        $out .= $this->box($notice, 'coursebox');
                        $out .= $this->box_end();

                        $program_idyears->MoveNext();
                    }

                    while (!$coursegroups->EOF) {

                        $coursegroup_id = $coursegroups->fields['coursegroup_id'];

                        $term = '';
                        $year = '';

                        $select = "SELECT * FROM `$CFG->sisdb_name`.`coursegroup` WHERE `id` = '$coursegroup_id'";
                        $coursegroup = $sisdb->Execute($select)->fields;

                        switch (substr($coursegroup['term'],-1)) {
                            case '1' : $term = get_string('winter', 'block_cegep'); break;
                            case '2' : $term = get_string('summer', 'block_cegep'); break;
                            case '3' : $term = get_string('autumn', 'block_cegep'); break;
                        }
                        $year = substr($coursegroup['term'],0,4);

                        $notice = '';

                        $notice .= '<strong>'.get_string('semester','block_cegep').'</strong> : ' . $term . '&nbsp;' . $year . '<br />';
                        $notice .= '<strong>'.get_string('coursecode','block_cegep').'</strong> : ' . $coursegroup['coursecode'] . '<br />';
                        $notice .= '<strong>'.get_string('coursegroupnumber','block_cegep').'</strong> : ' . $coursegroup['group'] . '<br /><br />';

                        $select = "SELECT * FROM `$CFG->enrol_remoteenroltable` WHERE `$CFG->enrol_remotecoursefield` = '$c->idnumber' AND `coursegroup_id` = '$coursegroup[id]' AND `$CFG->enrol_remoterolefield` = '$CFG->block_cegep_studentrole' ORDER BY `$CFG->enrol_remoteuserfield` ASC";

                        $table = $this->get_studentlist_enrolmenttable($select);

                        if (count($table->data) > 0) {
                            $notice .= html_writer::table($table);
                            $notice .= '<br /><strong>'.get_string('total').'</strong> : ' . count($table->data);
                        }
                        else if (is_null($coursegroup['id'])) {
                            $coursegroups->MoveNext();
                            continue;
                        }
                        else {
                            $notice .= get_string('nocoursegroupsenrolled','block_cegep');
                        }

                        $body .= $this->box_start();
                        $body .= $this->box($notice, 'coursebox');
                        $body .= $this->box_end();

                        $coursegroups->MoveNext();
                    }
                }
            }
        }

        if (empty($body)) {
            $body .= $this->container(get_string('nocoursegroupsenrolled', 'block_cegep'));
        }

        $out .= $this->header();
        $out .= $this->get_tabs($context, $currenttab, array('id' => $course->id));
        $out .= $this->heading(get_string('studentlist','block_cegep'), 3);
        $out .= $body;
        $out .= $this->footer();

        return $out;
    }

    protected function get_studentlist_enrolmenttable($select) {
        global $CFG, $DB, $enroldb, $sisdb;

        $table = new html_table();
        $table->class = 'flexible';
        $table->width = '100%';
        $table->cellpadding = 4;
        $table->cellspacing = 3;
        $table->align = array('left','left','left');
        $table->head = array(get_string('username'),get_string('lastname'),get_string('firstname'),get_string('program','block_cegep'));
        $table->data = array();

        // Obtenir la liste des étudiants
        $enrolments_rs = $enroldb->Execute($select);

        // Inscription des étudiants
        $lastnames = array();
        while ($enrolments_rs and !$enrolments_rs->EOF) {
            $select = "SELECT * FROM `$CFG->sisdb_name`.`student` WHERE `username` = '" . $enrolments_rs->fields[$CFG->enrol_remoteuserfield] . "'";
            $student_rs = $sisdb->Execute($select);
            if ($student_rs && $student_rs->RecordCount() == 1) {
                $student_sisdb = $student_rs->fields;
                $student_moodle = $DB->get_record('user', array($CFG->enrol_localuserfield => $student_sisdb['username']), 'id');
                if ($student_moodle) {

                    $select = "SELECT `title` FROM `$CFG->sisdb_name`.`program` WHERE `id` = '" . $student_sisdb['program_id'] . "'";
                    $program = $sisdb->Execute($select)->fields;

                    $table->data[] = array('<a href="'.$CFG->wwwroot.'/user/view.php?id='.$student_moodle->id.'" title="'.get_string('accessuserprofile','block_cegep').'">'.$student_sisdb['username'].'</a>', $student_sisdb['lastname'], $student_sisdb['firstname'], $program['title'] . " (" . $student_sisdb['program_id'] . ")");
                    $lastnames[] = $student_sisdb['lastname'];
                } else {
                    $select = "SELECT `title` FROM `$CFG->sisdb_name`.`program` WHERE `id` = '" . $student_sisdb['program_id'] . "'";
                    $program = $sisdb->Execute($select)->fields;

                    $table->data[] = array($student_sisdb['username'], $student_sisdb['firstname'], $student_sisdb['lastname'], $program['title'] . " (" . $student_sisdb['program_id'] . ")");
                    $lastnames[] = $student_sisdb['lastname'];
                }
            }
            $enrolments_rs->MoveNext();
        }
        array_multisort($lastnames, SORT_ASC, $table->data);
        return $table;
    }

    /**
     * Get tabs
     *
     * @param \context $context
     * @param string $selected
     * @param array $params page parameters
     * @return string
     */
    protected function get_tabs($context, $selected, $params = array()) {
        global $CFG;
        $tabs = array();
        $url = $CFG->wwwroot . '/blocks/cegep/block_cegep_enrolment.php';

        $studentlist = new \moodle_url($url, $params);
        $tabs[] = new tabobject('studentlist', $studentlist, get_string('studentlist','block_cegep'));

        $enrol = new \moodle_url($url, $params + array('a' => 'enrol'));
        $tabs[] = new tabobject('enrol', $enrol, get_string('enrol','block_cegep'));

        $unenrol = new \moodle_url($url, $params + array('a' => 'unenrol'));
        $tabs[] = new tabobject('unenrol', $unenrol, get_string('unenrol','block_cegep'));

        $syscontext = \context_system::instance();
        if (is_siteadmin() || has_capability('block/cegep:enroladmin_program', $syscontext)) {

            $enrolprogram = new \moodle_url($url, $params + array('a' => 'enrolprogram'));
            $tabs[] = new tabobject('enrolprogram', $enrolprogram, get_string('enrolprogram','block_cegep'));

            $unenrolprogram = new \moodle_url($url, $params + array('a' => 'enrolprogram'));
            $tabs[] = new tabobject('unenrol', $unenrolprogram, get_string('unenrolprogram','block_cegep'));
        }

        return '<div class="groupdisplay">' . $this->tabtree($tabs, $selected) . '</div>';
    }


}
