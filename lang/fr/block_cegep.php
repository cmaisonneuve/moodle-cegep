<?php

$string['pluginname'] = 'Admin CEGEP';

// Block menu (course)
$string['admincegep'] = 'Admin CEGEP';
$string['enrolment'] = 'Inscriptions';
$string['delete'] = 'Détruire';

// Block menu (MyMoodle)
$string['missingcoursetitle'] = 'Le titre du cours n\'est encore pas disponible dans banque de cours du registraire.';

// Tabs
$string['studentlist'] = 'Liste d\'étudiants';
$string['enrol'] = 'Inscrire groupe-cours';
$string['unenrol'] = 'Désinscrire groupe-cours';
$string['enrolprogram'] = 'Inscrire un programme';
$string['unenrolprogram'] = 'Désinscrire un programme';

// Common
$string['summer'] = 'Été';
$string['autumn'] = $string['fall'] = 'Automne';
$string['winter'] = 'Hiver';
$string['year'] = 'Année';
$string['semester'] = $string['term'] = 'Session';
$string['coursegroup'] = $string['section'] = 'Groupe-cours';
$string['number'] = 'Nombre';
$string['comments'] = "Commentaires";
$string['create'] = "Créer";
$string['teacher'] = 'Enseignant';
$string['teachers'] = 'Enseignants';

// My moodle page
$string['cegepsection'] = 'Section';
$string['cegepenrolled'] = 'Enrolled Sections:';
$string['cegepenrolledstud'] = 'Section(s):';
$string['cegepvisible'] = 'Visible to students:';
$string['cegepnosections'] = 'None';
$string['cegepvisibleyes'] = 'Yes';
$string['cegepvisibleno'] = 'No';
$string['cegepeditsettings'] = 'Edit settings';

// Student list page
$string['studentlisttitle'] = 'Liste d\'étudiants inscrits dans ce cours';
$string['childcoursetitle'] = 'Titre du cours descendant';
$string['coursecode'] = 'Code du cours';
$string['coursegroupnumber'] = 'Numéro du groupe-cours';
$string['program'] = 'Programme';
$string['nocoursegroupsenrolled'] = 'Aucun groupe-cours n\'est inscrit dans ce cours';
$string['accessuserprofile'] = 'Accéder au profil de l\'utilisateur';
$string['nostudentsenrolled'] = 'Il n\'y a aucun étudiant inscrit à ce cours.';

// Enrol form
$string['coursegroupenrolled'] = '<strong>Inscription complétée avec succès.</strong><br /><br />Voici les étudiants ajoutés au cours :<br /><br />{$a}<br /><br />Bonne session!<br /><br />';
$string['enrolanother'] = 'Inscrire un autre groupe-cours?';
$string['make_visible'] = 'Rendre ce cours disponible aux étudiants';
$string['enrolhelp'] = "Choisissez les groupes-cours à inscrire dans ce cours. Notez que si vous désirez avoir <b>un cours distinct pour chaque groupe-cours</b>, vous devez n'inscrire qu'un seul groupe-cours ici et créer un cours pour vos autres groupes-cours.";
$string['enrolcoursegroup'] = 'Inscrire un groupe-cours';
$string['nocoursegroupsavailable'] = "Aucun groupe-cours n'est disponible pour inscription à ce cours.";
$string['nocoursegroupselected'] = 'Veuillez choisir un groupe à inscrire.';
$string['alreadyenrolled'] = 'déjà inscrit';
$string['coursegrouphasnostudents'] = 'Aucun étudiant n\'est inscrit dans ce groupe-cours.';
$string['creategroups'] = 'Créer un groupe Moodle par groupe-cours';
$string['selectmultiplehelp'] = 'Ctrl-clic pour sélectionner plusieurs à la fois.';

// Unenrol form
$string['students'] = 'étudiants';
$string['unenrolbutton'] = 'Désinscrire';
$string['coursegroupunenrolled'] = '<strong>Désinscription complétée avec succès.</strong><br /><br />{$a} étudiants enlevés du cours.<br /><br />';

// Program enrol form
$string['programenrolled'] = '<strong>Inscription complétée avec succès.</strong><br />Bonne session!<br /><br />';
$string['programyear'] = 'Année';
$string['programyear1'] = '1ère année';
$string['programyear2'] = '2e année';
$string['programyear3'] = '3e année';

// Program unenrol form
$string['unenrolprogrambutton'] = 'Désinscrire';
$string['programunenrolled'] = '<strong>Désinscription complétée avec succès.</strong><br /><br />{$a} étudiants enlevés du cours.<br /><br />';

// Validation
$string['specifyyear'] = 'Veuillez spécifier l\'année.';
$string['specifysemester'] = 'Veuillez spécifier la session.';
$string['specifycoursegroup'] = 'Veuillez spécifier le groupe-cours.';
$string['specifyprogram'] = 'Veuillez spécifier le programme.';
$string['specifyprogramyear'] = 'Veuillez spécifier l\'année.';
$string['semesterunavailable'] = 'La session spécifiée n\'est pas disponible dans le système.';
$string['coursegroupsixnumbersonly'] = 'Le numéro du groupe-cours doit comporter six chiffres.';
$string['coursegroupalreadyenrolled'] = 'Le groupe-cours spécifié est déjà inscrit à ce cours.';
$string['coursegroupnotenrolled'] = 'Le groupe-cours spécifié n\'est pas inscrit à ce cours.';
$string['coursegroupunavailable'] = 'Le groupe-cours spécifié n\'est pas disponible dans le système.';
$string['coursegroupinvalid'] = 'Le groupe-cours spécifié est invalide.';
$string['coursecodeinvalid'] = 'Le code de cours spécifié est invalide.';
$string['programinvalid'] = 'Le programme spécifié est invalide.';
$string['programalreadyenrolled'] = 'Le programme spécifié est déjà inscrit à ce cours.';

// Settings
$string['studentrole'] = 'Rôle étudiant';
$string['studentrole_help'] = 'Rôle à assigner aux étudiants dans la base de données externe.';
$string['cegepname'] = 'Nom du CEGEP';
$string['cegepname_help'] = 'Nom du CEGEP en minuscule pour les lib';
$string['cronpassword'] = 'Mot de passe cron';
$string['cronpassword_help'] = 'Entrer un mot de passe pour le mode automatique via cron. En appelant le script, ajouter cette chaîne au paramètre \'password\'. Si e champ est laissé vide, il faudra déclencher la synchronisation manuellement en tant qu\'administrateur du site.';
$string['sisdb_heading'] = 'Base de données du SIS';
$string['sisdb_help'] = 'Informations d\'accès pour la base de données intermédiaire du système d\'information scolaire de l\'institution.';
$string['sisdb_type'] = 'Type de BD';
$string['sisdb_host'] = 'Hôte';
$string['sisdb_name'] = 'Nom de la BD';
$string['sisdb_user'] = 'Nom d\'utilisateur';
$string['sisdb_pass'] = 'Mot de passe';
$string['sisdb_logging'] = 'Journaliser les transaction de la base de données';
$string['sisdb_sync_csv'] = 'Synchroniser la BD avec un fichier CSV';
$string['sisdbsource_help'] = 'Informations d\'accès pour la base de données du système d\'information scolaire de l\'institution. Cette base de données sera utilisée comme source d\'informations. (Clara, Datamart, etc.)';
$string['sisdbsource_type'] = 'Type de BD';
$string['sisdbsource_host'] = 'Hôte';
$string['sisdbsource_name'] = 'Nom de la BD';
$string['sisdbsource_user'] = 'Nom d\'utilisateur';
$string['sisdbsource_pass'] = 'Mot de passe';
$string['sisdb_sync_csv'] = 'Synchroniser la BD avec un fichier CSV';
$string['sisdb_sync_db'] = 'Synchroniser la BD avec la BD source';

$string['autotopic'] = "Thème 0 automatique";
$string['autotopic_help'] = "Inscrit automatiquement le titre du cours et la liste d'enseignants dans le thème 0 du cours.";

$string['autogroups'] = "Créer les groupes";
$string['autogroups_help'] = "Crée automatiquement des groupes dans le cours Moodle correspondant à chaque groupecours inscrit.";

// Course creation
$string['coursecreatesuccess'] = "Le cours a été créé avec succès. Cliquez sur 'Annuler' pour accéder à la page d'acceuil du cours sans inscrire de groupe.";
$string['coursecreate'] = 'Créer un cours';

// Course deletion
$string['deletedenrolments'] = "Supprimé - Inscriptions";

// Errors
$string['errorenroldbnotavailable'] = 'Le module d\'inscription base de données externe doit être activé pour le fonctionnement de ce module.';
$string['erroractionnotavailable'] = 'Cette action n\'est pas disponible pour ce cours.';
$string['errormustbeteacher'] = 'Cette page est disponible uniquement pour les enseignants.';
$string['errormustbeteachercourse'] = 'Vous devez être enseignant dans ce cours.';
$string['errormustbeadmin'] = 'Cette page est disponible uniquement pour les administraeurs du système.';
$string['errorcreatingcourse'] = "Une erreur s'est produite lors de la création du cours!";
$string['errorimportingstudentlist'] = 'Une erreur s\'est produite lors de l\'importation de la liste d\'étudiants!';
$string['errordeletingenrolment'] = 'Une erreur s\'est produite lors de la suppression des inscriptions au cours!';

// SIS DB maintenance
$string['sisdb_maintenance'] = 'Entretient de la BD SIS';
$string['sisdb_sync'] = 'Synchroniser';
$string['sisdb_prune'] = 'Élaguer';

// Capabilities
$string['cegep:enroladmin_course'] = "Assigner un groupe dans un cours Moodle sans contrainte";
$string['cegep:enroladmin_program'] = "Assigner un programme à un cours Moodle";

?>
