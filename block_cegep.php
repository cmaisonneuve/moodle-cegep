<?php

include_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/blocks/cegep/lib.php');

class block_cegep extends block_list {
    function init() {
        $this->title = get_string('admincegep', 'block_cegep');
    }

    function has_config() {return true;}

    function applicable_formats() {
        return array(
            'course-view' => true,
            'my-index' => true,
            'site-index' => true,
        );
    }

    function get_content() {
        global $CFG, $OUTPUT;

        if ($this->content !== NULL) {
            return $this->content;
        }

        $this->content = new \stdClass;
        $this->content->items = array();
        $this->content->icons = array();
        $this->content->footer = '';

        $context = $this->page->context->get_course_context(false);

        // Show MyMoodle block
        if ($this->page->course->id == SITEID) {
            $this->title = get_string('coursecreate', 'block_cegep');
            $this->content = cegep_local_get_create_course_menu($this->content);
        }
        // Show course block
        elseif (has_capability('moodle/course:update', $context)) {
            $url = new moodle_url('/blocks/cegep/block_cegep_enrolment.php');
            $url->params(array('id' => $this->page->course->id));
            $icon = $OUTPUT->pix_icon('i/groups', '');
            $this->content->items[] = '<a href="'.$url->out().'">'.$icon.get_string('enrolment', 'block_cegep').'</a>';
        }

        return $this->content;
    }

}

?>
