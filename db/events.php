<?php

defined('MOODLE_INTERNAL') || die();

$observers = array (
    array (
        'eventname'   => '\core\event\course_deleted',
        'callback'    => '\block_cegep\eventobservers::course_deleted',
        'includefile' => '/blocks/cegep/lib.php',
        'internal'    => false, // This means that we get events only after transaction commit.
        'priority'    => 1,
    ),
);
